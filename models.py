from pydantic import BaseModel

class User(BaseModel):
    username: str
    hashed_password: str
    fullname: str
    npm: str
    client_id: str
    client_secret: str
    login_at: str
    access_token: str
    refresh_token: str