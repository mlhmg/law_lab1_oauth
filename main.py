from fastapi import Depends, FastAPI, HTTPException, status, Form, Security
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from fastapi.responses import JSONResponse
from datetime import datetime
from models import User
import hashlib
import uvicorn


app = FastAPI()

users_db = {
    "usertest": {
        "username": "usertest",
        "hashed_password": "hashedpass123",
        "fullname": "user test",
        "npm": "1806205533",
        "client_id": "2222",
        "client_secret": "1111",
        "login_at": "",
        "access_token": "",
        "refresh_token": ""
    }
}

token_dict = {}

def hash_password(password):
    return "hashed"+password


def generate_token(username, token_type):
    curr_time = get_curr_time()
    payload = token_type + username + "/" + curr_time
    token = hashlib.sha1(payload.encode()).hexdigest()
    token_dict[token] = username
    return token


def get_curr_time():
    now = datetime.now()
    curr_time = now.strftime("%m/%d/%Y, %H:%M:%S")
    return curr_time


def authenticate_user(username: str, password: str):
    try:
        user = users_db[username]
    except:
        return False

    if user["hashed_password"] != hash_password(password):
        return False
    return user

def client_validation(user, client_id, client_secret):
    if client_id != user["client_id"]:
        return False
    if client_secret != user["client_secret"]:
        return False
    return True

@app.post("/oauth/token")
async def token(
    username: str = Form(...),
    password: str = Form(...),
    grant_type: str = Form(...),
    client_id: str = Form(...),
    client_secret: str = Form(...)
):
    user = authenticate_user(username, password)
    if not user:
        return JSONResponse(
            status_code=401,
            content={
                "error": "invalid_request",
                "error_description": "Invalid username or password"
            }
        )

    if not client_validation(user, client_id, client_secret):
        return JSONResponse(
            status_code=401,
            content={
                "error": "invalid_request",
                "error_description": "Invalid client id or client secret"
            }
        )

    if grant_type != "password".casefold():
        return JSONResponse(
            status_code=401,
            content={
                "error": "invalid_request",
                "error_description": "Wrong grant type"
            }
        )

    access_token = generate_token(username, "access")
    refresh_token = generate_token(username, "refresh")
    user["access_token"] = access_token
    user["refresh_token"] = refresh_token

    curr_time = get_curr_time()
    user["login_at"] = curr_time

    response = {
        "access_token": access_token,
        "expires_in": 300,
        "token_type": "Bearer",
        "scope": None,
        "refresh_token": refresh_token
    }
    return response


@app.post("/oauth/resource")
async def resource(credentials: HTTPAuthorizationCredentials = Security(HTTPBearer())):
    access_token = credentials.credentials
    try:
        username = token_dict[access_token]
    except:
        return JSONResponse(
            status_code=401,
            content={
                "error": "invalid_request",
                "error_description": "Unauthorized"
            }
        )
    user_dict = users_db.get(username)
    user = User(**user_dict)

    if(access_token == user.access_token):
        time_format = "%m/%d/%Y, %H:%M:%S"
        curr_time = get_curr_time()
        expire = 300 - (datetime.strptime(curr_time, time_format) -
                        datetime.strptime(user.login_at, time_format)).total_seconds()
        if expire <= 0:
            response = JSONResponse(
                status_code=401,
                content={
                    "error": "invalid_request",
                    "error_description": "Login timeout expired"
                }
            )
        else:

            response = {
                "access_token": user.access_token,
                "client_id": user.client_id,
                "user_id": user.username,
                "full_name": user.fullname,
                "npm": user.npm,
                "expires": expire,
                "refresh_token": user.refresh_token
            }
    else:
        response = JSONResponse(
            status_code=401,
            content={
                "error": "invalid_request",
                "error_description": "Unauthorized"
            }
        )

    return response

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
